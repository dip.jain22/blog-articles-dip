# A Journey To Self-Worth

*Author: Miheera Nikam*

## This article is about **self-worth** and how we can install the practice of uplifting and maintaining self-worth for a better life.

At the heart of who we are is a big idea that guides how we live: **self-worth**. It means understanding that we're valuable just because we're here. Building a strong sense of self-worth isn't just a mind game—it's a journey to feeling good about ourselves and finding happiness by being true to who we are.


In our lives, we get entangled in the chain of societal expectations and start considering those standards as ideal. We start raising concerns over our physical appearance, social status, the possessions we have, and even the little quirks in our personalities. We tend to forget that we are, by the end of the day, complex individuals who are unique in our way and mustn't be ashamed to accept those differences.

>"You, as much as anybody in the entire universe, deserve your love and affection" - *Buddha*


By constantly comparing themselves with others and hyper-focusing on their failures and mistakes of the past, one brings a sense of uneasiness and disappointment which, in turn, begins a vicious cycle of regret and guilt carried on their entire life and never having a break-out from it till they realise where the actual problem lies.  

![Your worth](/images/your_worth.webp)

Our unconditional self-worth is distinct from our abilities and accomplishments. It’s not about comparing ourselves to others; it’s not something that we can have more or less of. Unconditional self-worth is the sense that you deserve to be alive, to be loved and cared for. To take up space.


Ads tell us that we need to buy things to be loved, accepted, or to succeed. Our educational system teaches us that our worthiness as students is based on our grades or test scores. Our parents may have implied they’d love us more if we made it to the top institutions of the country. Those of us who’ve experienced abuse, sexual assault, and trauma may question our personhood and very right to exist. And, as social media pervades our lives, we have also begun to feel that our worthiness is based on the number of followers we have and the likes we get.


Whatever the cause, for many of us our self-worth is tied to our accomplishments and possessions. As soon as we fail or lose approval, we experience low self-worth.

![What we think vs what we are](/images/we-are-much-more-than-we-think.webp)

Unconditional self-worth is the antidote to low self-worth. It is a way out of self-criticism, shame, and unhealthy behavior. It is a way out of depression, anxiety, and substance abuse. It is time for us to base our worth on the fact that we are human to cultivate a worth that persists even when life does not go as we hoped.


*So what keeps so many of us from cultivating unconditional self-worth?*


Some people might fear that if they get too satisfied with themselves, they won’t be motivated to grow and change. Others could feel that accepting themselves as worthy would be arrogant. And some may simply believe that feeling worthy is just not possible.


It's easy to get caught up in the hustle for external approval, but true self-worth comes from within. It's the quiet confidence that says, "**I am enough**." It's about setting boundaries, practicing self-care, and celebrating the small victories on your path.

![Bad vibes](/images/bad_vibes.webp)

Mistakes? They're not roadblocks; they're stepping stones. Each stumble is a chance to learn, grow, and become a wiser version of yourself. Remember, your worth isn't contingent on perfection; it thrives in authenticity.


Surround yourself with people who uplift you, activities that bring you joy, and thoughts that empower you. Self-worth is a daily practice, a journey of self-discovery, and a commitment to being kind to yourself, even on the tough days.


So, chin up, my friend! You are a constellation of strengths, a masterpiece in the making. Own your worth, wear it like a crown, and let it be the guiding star on your incredible journey.

[ref1](https://ideas.ted.com/how-to-cultivate-a-sense-of-unconditional-self-worth/)

[ref2](https://bigthink.com/neuropsych/scientific-ways-to-boost-confidence/)

## Moderator notes:

- You may make certain words *italic*, **bold** depending on the emphasis.

- I've updated your image links from external to internal.

- I've put your 'lord Buddha' quote within `markdown Blockquote`.

### Changes to make

- Include references/links directly in the text. For example:
[Markdown](https://en.wikipedia.org/wiki/Markdown)  is a lightweight markup language for creating [formatted text](https://en.wikipedia.org/wiki/Formatted_text) using a plain-text editor.

- Change your title from uppercase to title case. Title need not be **bold**.

- You may include citations, links to other articles, studies to support your points.

- To separate paragraphs press enter twice.  